#include "player.h"
Player1337::Player1337(const char *_name) {
  this->name = _name;
  num = 0;
  nC = true;
}
void Player1337::YouTurn(bool) {}
void Player1337::PutCard() {
  Player1337::sort();
  if (Dealer::GetCurrentHeadTrik() == 0) { //Первый ход
		Dealer::Attack(cards[0]);
		cards[0] = cards[num-1];
		cards[num - 1] = nullptr;
		num--;
		return;
  } else { //Подкидываем
		for (int i = 0; i < num; i++)
			if (Dealer::RankIndex(cards[i]) == Dealer::RankIndex(Dealer::GetLastCard()) ||
			  	Dealer::RankIndex(cards[i]) == Dealer::RankIndex(Dealer::GetLastDefendCard())){
						Dealer::Attack(cards[i]);
						cards[i] = cards[num - 1];
						cards[num - 1] = nullptr;
						num--;
						return;
			}
		Dealer::Attack(Dealer::GetNocard());
	}
}
void Player1337::TakeCards() {
  for (int i = 0; i < Dealer::GetCurrentHeadTrik(); i++)
	for (int _i = 0; _i < 2; _i++) {
		if (i == Dealer::GetCurrentHeadTrik() - 1 && _i == 1) continue;
    Card *tmp = Dealer::GetheadTrick()[_i][i];
    if (Dealer::RankIndex(tmp) < 300) {
      cards[num] = tmp;
      num++;
    }
  }
  Player1337::sort();
}
void Player1337::GetHeadTrick() {
  if (Dealer::RankIndex(Dealer::GetLastCard()) == NOCARD)
    Dealer::Defend(Dealer::GetNocard());
  else {
    for (int i = 0; i < num; i++) {
      if (Dealer::SuitIndex(cards[i]) ==
          Dealer::SuitIndex(Dealer::GetLastCard()) &&
          Dealer::RankIndex(cards[i]) >
          Dealer::RankIndex(Dealer::GetLastCard())) {
			        Dealer::Defend(cards[i]);
			        cards[i] = cards[num - 1];
			        cards[num - 1] = nullptr;
			        num--;
			        return;
      }
    }
    Dealer::Defend(Dealer::GetPas());
  }
}
void Player1337::TakeOneCard(Card *&nc) {
  if (nc != Dealer::GetNocard()) {
    cards[num] = nc;
    num++;
  } else nC = false;
  Player1337::sort();
}
void Player1337::ShowCards() {
  cout << name << "\'s cards:" << endl;
  for (int i = 0; i < num; i++)
  	cout <<"("<< i + 1 << ") " << Dealer::SuitName(cards[i])[0] << "-" <<
  				 Dealer::RankName(cards[i]) << "\t";
  cout<<endl<<endl;
}
bool Player1337::INeedCard() { return num<6 ? nC : false;  }
int Player1337::GetCardNum() { return num;                 }
void Player1337::sort() {
  for (int i = 0; i < num; i++) for (int j = 0; j < num - 1; j++)
  if (Dealer::RankIndex(cards[j]) > Dealer::RankIndex(cards[j + 1])) {
    Card *tmp;
    tmp = cards[j];
    cards[j] = cards[j + 1];
    cards[j + 1] = tmp;
  }
}
