#pragma once
#include <cstring>
#include <iostream>
#include "dealer.h"
using namespace std;
class PlayerAbstract{
public:
	virtual ~PlayerAbstract() {};
	//ïîëó÷àåò ïðèçíàê "ìîé õîä"
	virtual void YouTurn(bool) = 0;
	//èãðîê êëàäžò êàðòó íà ñòîë (headTrick[0][*])
	virtual void PutCard() = 0;
	// çàáèðàåò âñå êàðòû ñî ñòîëà
	virtual void TakeCards() = 0;
	// îòáèâàåò êàðòó (êëàäžò êàðòó â (headTrick[1][*])
	virtual void GetHeadTrick() = 0;
	//âçÿë îäíó êàðòó
	virtual void TakeOneCard(Card * &nc) = 0;
	// âûâåë ñâîè êàðòû íà ýêðàí ðàíã (îäèí/äâà ñèìâîëà, ìàñòü - ñèìâîë)
	virtual void ShowCards() = 0;
	// âîçâðàùàåò èñòèíó, åñëè íà ðóêàõ êàðò ìåíüøå 6
	virtual bool INeedCard() = 0;
	// âîçâðàùàåò ÷èñëî êàðò íà ðóêàõ
	virtual int GetCardNum() = 0;
	virtual void sort()=0;
};

class Player1337 : public PlayerAbstract{
public:
	const char* name;
	Card* cards[54];
	int num;
	bool nC;
public:
	Player1337(const char *name);
	~Player1337(){};
	virtual void YouTurn(bool);
	virtual void PutCard();
	virtual void TakeCards();
	virtual void GetHeadTrick();
	virtual void TakeOneCard(Card * &nc);
	virtual void ShowCards();
	virtual bool INeedCard();
	virtual int GetCardNum();
	virtual void sort();
};
