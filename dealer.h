#pragma once

enum { PAS = 300, NOCARD = 400 };
//PAS - ïðèçíàê ïðîïóñêà õîäà (íå÷åì õîäèòü èëè êðûòü)
//NOCARD - ïðèçíàê ïðîïóñêà õîäà (íåò êàðò íà ðóêàõ)

static const char *suits[]     = { "Clubs", "Spades", "Hearts", "Diamonds" };
static const char *suitsSymb[] = { "\x5", "\x6", "\x3", "\x4" };
static const char *ranks[] = { "2", "3", "4", "5", "6", "7", "8", "9", "10", "Jack", "Queen", "King", "Ace"};

struct Card
{
private:
	int suit;
	int rank;

	bool operator >(const Card &card);
	friend class Dealer;

	Card(int suit = -1, int rank = -1);
public:

};


class Dealer
{
public:
	static const int maxSuits = 4;
	static const int maxRanks = 13;
	static const int maxTrick = 6;
private:
	static int currentCard;
	static Card *trump;  // êàðòà - êîçûðü
	static Card *noCard, *pasCard; // êàðòà - ïðèçíàê "íåò êàðò" è "ïàñ"
	static Card deck[maxSuits*maxRanks]; //êîëîäà

	static bool tableRanks[maxRanks];   // ðàíãè êàðò, ïðèñóòñòâóþùèõ íà ñòîëå

	static int currentHeadTrik; //íîìåð õîäà â êîíó
	static Card *headTrick[2][maxTrick]; // ñòîë [0] - õîä èãðîêà, [1] - îòáîé êàðòû
	static void GenerateDeck();



public:

	//ïåðåìåøàòü êîëîäó - èíèöèàëèçèðóåò êîëîäó è âñå ïåðåìåííûå.
	//âûáèðàåò êîçûðÿ
	static void ShuffleDec();
	//âçÿòü êàðòó èç êîëîäû. âîçâðàùàåò true, åñëè êàðòû åùå îñòàëèñü.
	static bool GetCard(Card * &outCard);
	//âîçâðàùàåò òåêóùåãî êîçûðÿ â ñòðóêòóðå Card (èìååò çíà÷åíèå òîëüêî ìàñòü).
	static Card *GetTrump();
	// âîçâðàùàåò ÷èñëî âûøåäøèõ èç êîëîäû êàðò
	static int getcurrentCard();

	//âîçâðàùàåò óêàçàòåëü íà ñòîë

	static Card * (*GetheadTrick())[maxTrick];

	//âîçâðàùàåò ñòðîêîâûé ëèòåðàë ñîäåðæàùèé ìàñòü èëè äîñòîèíñòâî êàðòû
	static const char * SuitName(const Card *card);
	static const char * RankName(const Card *card);

	// * âîçâðàùàåò èíäåêñ ñîîòâåòñòâóþùèé ìàñòè èëè äîñòîèíñòâó êàðòû
	static int SuitIndex(const Card *card);
	static int RankIndex(const Card *card);
    // âîçâðàùàåò íîìåð õîäà (0-5)
	static int GetCurrentHeadTrik();
	// Ïðîâåðÿåò âîçìîæåí ëè ñëåäóþùèé õîä (õîäîâ<6,îòáèâàþùèéñÿ èãðîê íå ñïàñîâàë)
	static bool NextTrikEnable();


	// * âûâîäèò íà ýêðàí êàðòó èëè ñòîë
	static void ShowCard(const Card *card);
	static void ShowTable();
	//* âîçâðàùàåò óêàçàòåëü íà êàòðó "ïàñ" èëè "íåò êàðòû"
	static Card *GetPas();
	static Card *GetNocard();
	// âîçâðàùàåò óêàçàòåëü íà ïîñëåäíþþ êàðòó ñ êîòîðîé õîäèëè è êîòîðîé îòáèâàëèñü
	static Card *GetLastCard();
	static Card *GetLastDefendCard();


	//î÷èùàåò ñòîë
	static void ClearTable();
	//ïðîâåðÿåò êàðòû íà ñòîëå íà êîððåêòíîñòü
	static bool CheckHeadTrick();

	//àòàêîâàòü èãðîêà êàðòîé card
	static void Attack(Card *card);
	//ïîêðûòü àòàêóþùóþ êàðòó, êàðòîé card
	static void Defend(Card *card);

	~Dealer();
};
